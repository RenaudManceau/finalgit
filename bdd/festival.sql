-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 14 jan. 2022 à 16:28
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `festival`
--

-- --------------------------------------------------------

--
-- Structure de la table `artist`
--

DROP TABLE IF EXISTS `artist`;
CREATE TABLE IF NOT EXISTS `artist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `start_hour` varchar(50) DEFAULT NULL,
  `end_hour` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `artist`
--

INSERT INTO `artist` (`id`, `name`, `start_hour`, `end_hour`) VALUES
(5, 'test', '2021-12-06 12:29:00.000', '2021-12-06 16:29:00.000'),
(6, 'Artiste de fou', '2022-02-17 14:26:00.000', '2022-02-25 17:26:00.000');

-- --------------------------------------------------------

--
-- Structure de la table `festival`
--

DROP TABLE IF EXISTS `festival`;
CREATE TABLE IF NOT EXISTS `festival` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `festival`
--

INSERT INTO `festival` (`id`, `name`, `city`, `country`) VALUES
(1, 'festoche', 'la chaussaire', 'France');

-- --------------------------------------------------------

--
-- Structure de la table `festivalartist`
--

DROP TABLE IF EXISTS `festivalartist`;
CREATE TABLE IF NOT EXISTS `festivalartist` (
  `idFestival` int(11) NOT NULL,
  `idArtist` int(11) NOT NULL,
  KEY `idArtist` (`idArtist`),
  KEY `idFestival` (`idFestival`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(50) NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `mail`, `password`) VALUES
(1, 'manceaurenaud49@gmail.com', 'salut'),
(2, 'r@gmail.com', '$2a$10$mqv8ktX1zT43dpQDOPr0tOEivaEYZy1eLOwZ6yVUsp77SRku5EHcG'),
(3, 'renaud.manceau@epsi.fr', '$2a$10$RvyhP9O4bHPwfBmQPJ324evLbrdlIr.Ahe/hgUtmyaKTsHgoaXqCq');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `festivalartist`
--
ALTER TABLE `festivalartist`
  ADD CONSTRAINT `idArtist` FOREIGN KEY (`idArtist`) REFERENCES `artist` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idFestival` FOREIGN KEY (`idFestival`) REFERENCES `festival` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
